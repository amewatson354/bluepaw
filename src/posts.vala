/*
 * Copyright (C) 2022  Dawid Duma (pixel96x) <contact@pixel96x.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022 Dawid Duma (pixel96x) <contact@pixel96x.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using GLib;
using Json;
using Soup;

namespace BluePaw
{
    namespace Posts
    {
        public enum FileExt
        {
            PNG,
            JPG,
            GIF,
            WEBM,
            SWF
        }

        [Flags]
        public enum Flags
        {
            PENDING,
            FLAGGED,
            NOTES_LOCKED,
            STATUS_LOCKED,
            RATING_LOCKED,
            DELETED
        }

        public enum Rating
        {
            SAFE,
            QUESTIONABLE,
            EXPLICIT
        }

        public struct Post
        {
            // general
            uint64 id;
            uint64 change;
            DateTime created_at;
            DateTime updated_at;
            Rating rating;
            int upvotes;
            int downvotes;
            uint favcount;
            // main file
            uint file_width;
            uint file_height;
            uint64 file_size;
            FileExt file_ext;
            string file_md5;
            string file_url;
            // sample file(s)
            bool has_sample;
            uint sample_width;
            uint sample_height;
            string sample_url;
            // preview file
            uint preview_width;
            uint preview_height;
            string preview_url;
            // tags
            string[]? artist_tags;
            string[]? copyright_tags;
            string[]? character_tags;
            string[]? species_tags;
            string[]? general_tags;
            string[]? lore_tags;
            string[]? meta_tags;
            string[]? invalid_tags;
            string[]? locked_tags;
            // others
            string[]? sources;
            uint[]? pools;
            uint64? parent;
            uint64[]? childrens;
            uint64? approver_id;
            uint64 uploader_id;
            string? description;
            uint comments;
            Flags flags;
        }

        private static bool parse_json_post_from_node(Json.Node post_node, out Post? post)
        {
            post = Post();
            unowned Json.Object obj = post_node.get_object();

            try {
                if (Json.Path.query("$..flags.pending", post_node).get_array().get_boolean_element(0))
                    post.flags += Flags.PENDING;
                if (Json.Path.query("$..flags.flagged", post_node).get_array().get_boolean_element(0))
                    post.flags += Flags.FLAGGED;
                if (Json.Path.query("$..flags.note_locked", post_node).get_array().get_boolean_element(0))
                    post.flags += Flags.NOTES_LOCKED;
                if (Json.Path.query("$..flags.status_locked", post_node).get_array().get_boolean_element(0))
                    post.flags += Flags.STATUS_LOCKED;
                if (Json.Path.query("$..flags.rating_locked", post_node).get_array().get_boolean_element(0))
                    post.flags += Flags.RATING_LOCKED;
                if (Json.Path.query("$..flags.deleted", post_node).get_array().get_boolean_element(0))
                    post.flags += Flags.DELETED;
            } catch (Error err) {
                post = null;
                return false;
            }

            try {
                post.id = obj.get_int_member("id");
                post.change = obj.get_int_member("change_seq");
                post.created_at = new DateTime.from_iso8601(obj.get_string_member("created_at"), new TimeZone.local());
                post.updated_at = new DateTime.from_iso8601(obj.get_string_member("updated_at"), new TimeZone.local());
                string tmp = obj.get_string_member("rating");
                if (tmp == "s")
                    post.rating = Rating.SAFE;
                else if (tmp == "q")
                    post.rating = Rating.QUESTIONABLE;
                else
                    post.rating = Rating.EXPLICIT;
                post.upvotes = (int) Json.Path.query("$..score.up", post_node).get_array().get_int_element(0);
                //  post.upvotes = (int) obj.get_member("score").get_int();
                post.downvotes = (int) Json.Path.query("$..score.down", post_node).get_array().get_int_element(0);
                post.favcount = (uint) obj.get_int_member("fav_count");
                post.file_width = (uint) Json.Path.query("$..file.width", post_node).get_array().get_int_element(0);
                post.file_height = (uint) Json.Path.query("$..file.height", post_node).get_array().get_int_element(0);
                tmp = Json.Path.query("$..file.ext", post_node).get_array().get_string_element(0);
                if (tmp == "png")
                    post.file_ext = FileExt.PNG;
                else if (tmp == "jpg" || tmp == "jpeg")
                    post.file_ext = FileExt.JPG;
                else if (tmp == "gif")
                    post.file_ext = FileExt.GIF;
                else if (tmp == "webm")
                    post.file_ext = FileExt.WEBM;
                else
                    post.file_ext = FileExt.SWF;
                post.file_size = Json.Path.query("$..file.size", post_node).get_array().get_int_element(0);
                post.file_md5 = Json.Path.query("$..file.md5", post_node).get_array().get_string_element(0);
                if (!(Flags.DELETED in post.flags))
                    post.file_url = Json.Path.query("$..file.url", post_node).get_array().get_string_element(0);
                if (Json.Path.query("$..sample.has", post_node).get_boolean()) {
                    post.has_sample = true;
                    post.sample_width = (uint) Json.Path.query("$..sample.width", post_node).get_array().get_int_element(0);
                    post.sample_height = (uint) Json.Path.query("$..sample.height", post_node).get_array().get_int_element(0);
                    if (!(Flags.DELETED in post.flags))
                        post.sample_url = Json.Path.query("$..sample.url", post_node).get_array().get_string_element(0);
                } else {
                    post.has_sample = false;
                }
                post.preview_width = (uint) Json.Path.query("$..preview.width", post_node).get_array().get_int_element(0);
                post.preview_height = (uint) Json.Path.query("$..preview.height", post_node).get_array().get_int_element(0);
                if (!(Flags.DELETED in post.flags))
                    post.preview_url = Json.Path.query("$..preview.url", post_node).get_array().get_string_element(0);
                Json.Array tagsArray = Json.Path.query("$..tags.artist", post_node).get_array().get_array_element(0);
                if (tagsArray.get_length() > 0) {
                    post.artist_tags.resize((int) tagsArray.get_length());

                    for (int i = 0; i < tagsArray.get_length(); ++i)
                        post.artist_tags[i] = tagsArray.get_string_element(i);
                } else {
                    post.artist_tags = null;
                }

                tagsArray = Json.Path.query("$..tags.copyright", post_node).get_array().get_array_element(0);
                if (tagsArray.get_length() > 0) {
                    post.copyright_tags.resize((int) tagsArray.get_length());

                    for (int i = 0; i < tagsArray.get_length(); ++i)
                        post.copyright_tags[i] = tagsArray.get_string_element(i);
                } else {
                    post.copyright_tags = null;
                }

                tagsArray = Json.Path.query("$..tags.character", post_node).get_array().get_array_element(0);
                if (tagsArray.get_length() > 0) {
                    post.character_tags.resize((int) tagsArray.get_length());

                    for (int i = 0; i < tagsArray.get_length(); ++i)
                        post.character_tags[i] = tagsArray.get_string_element(i);
                } else {
                    post.character_tags = null;
                }

                tagsArray = Json.Path.query("$..tags.species", post_node).get_array().get_array_element(0);
                if (tagsArray.get_length() > 0) {
                    post.species_tags.resize((int) tagsArray.get_length());

                    for (int i = 0; i < tagsArray.get_length(); ++i)
                        post.species_tags[i] = tagsArray.get_string_element(i);
                } else {
                    post.species_tags = null;
                }

                tagsArray = Json.Path.query("$..tags.general", post_node).get_array().get_array_element(0);
                if (tagsArray.get_length() > 0) {
                    post.general_tags.resize((int) tagsArray.get_length());

                    for (int i = 0; i < tagsArray.get_length(); ++i)
                        post.general_tags[i] = tagsArray.get_string_element(i);
                } else {
                    post.general_tags = null;
                }

                tagsArray = Json.Path.query("$..tags.lore", post_node).get_array().get_array_element(0);
                if (tagsArray.get_length() > 0) {
                    post.lore_tags.resize((int) tagsArray.get_length());

                    for (int i = 0; i < tagsArray.get_length(); ++i)
                        post.lore_tags[i] = tagsArray.get_string_element(i);
                } else {
                    post.lore_tags = null;
                }

                tagsArray = Json.Path.query("$..tags.meta", post_node).get_array().get_array_element(0);
                if (tagsArray.get_length() > 0) {
                    post.meta_tags.resize((int) tagsArray.get_length());

                    for (int i = 0; i < tagsArray.get_length(); ++i)
                        post.meta_tags[i] = tagsArray.get_string_element(i);
                } else {
                    post.meta_tags = null;
                }

                tagsArray = Json.Path.query("$..tags.invalid", post_node).get_array().get_array_element(0);
                if (tagsArray.get_length() > 0) {
                    post.invalid_tags.resize((int) tagsArray.get_length());

                    for (int i = 0; i < tagsArray.get_length(); ++i)
                        post.invalid_tags[i] = tagsArray.get_string_element(i);
                } else {
                    post.invalid_tags = null;
                }

                tagsArray = Json.Path.query("$..locked_tags", post_node).get_array().get_array_element(0);
                if (tagsArray.get_length() > 0) {
                    post.locked_tags.resize((int) tagsArray.get_length());

                    for (int i = 0; i < tagsArray.get_length(); ++i)
                        post.locked_tags[i] = tagsArray.get_string_element(i);
                } else {
                    post.locked_tags = null;
                }
                Json.Array sourcesArray = Json.Path.query("$..sources", post_node).get_array().get_array_element(0);
                if (sourcesArray.get_length() > 0) {
                    post.sources.resize((int) sourcesArray.get_length());
                    for (int i = 0; i < sourcesArray.get_length(); ++i)
                        post.sources[i] = sourcesArray.get_string_element(i);
                } else {
                    post.sources = null;
                }
                Json.Array poolsArray = Json.Path.query("$..pools", post_node).get_array().get_array_element(0);
                if (poolsArray.get_length() > 0) {
                    post.pools.resize((int) poolsArray.get_length());
                    for (int i = 0; i < poolsArray.get_length(); ++i)
                        post.pools[i] = (uint) poolsArray.get_int_element(i);
                } else {
                    post.pools = null;
                }
                if (!Json.Path.query("$..relationships.parent_id", post_node).get_array().get_null_element(0))
                    post.parent = Json.Path.query("$..relationships.parent_id", post_node).get_array().get_int_element(0);
                else
                    post.parent = null;
                if (Json.Path.query("$..relationships.has_children", post_node).get_array().get_boolean_element(0)) {
                    Json.Array childrensArray = Json.Path.query("$..relationships.children", post_node).get_array().get_array_element(0);
                    post.childrens.resize((int) childrensArray.get_length());
                    for (int i = 0; i < childrensArray.get_length(); ++i)
                        post.childrens[i] = (uint) childrensArray.get_int_element(i);
                } else {
                    post.childrens = null;
                }
                if (!Json.Path.query("$..approver_id", post_node).get_array().get_null_element(0))
                    post.approver_id = Json.Path.query("$..approver_id", post_node).get_array().get_int_element(0);
                else
                    post.approver_id = null;
                post.uploader_id = (uint64) Json.Path.query("$..uploader_id", post_node).get_array().get_int_element(0);
                post.description = obj.get_string_member("description");
                post.comments = (uint) obj.get_int_member("comment_count");
            } catch (Error err) {
                post = null;
                return false;
            }

            return true;
        }

        public static async Post? fetch_post(string url, Cancellable? cancel)
        {
            Post post;
            uint8[]? data = new uint8[0];

            data = yield Network.fetch_data(url, cancel, Network.CallType.API);
            if (data == null)
                return null;

            var sb = new StringBuilder();
            foreach (var c in data)
                sb.append_c((char) c);

            if (!sb.str.validate())
                return null;

            var doc = new Parser.immutable_new();
            try {
                doc.load_from_data(sb.str);
                unowned Json.Node? root = doc.get_root();

                if (root == null || !root.get_object().has_member("post"))
                    return null;

                //  Json.Node node = Json.Path.query("$.post", root);

                if (parse_json_post_from_node(root, out post))
                    return post;
                else
                    return null;
            } catch (Error err) {
                return null;
            }
        }

        public static async Post[]? fetch_posts(string url, Cancellable? cancel)
        {
            Post[] posts;
            uint8[]? data = new uint8[0];

            data = yield Network.fetch_data(url, cancel, Network.CallType.API);
            if (data == null)
                return null;

            var sb = new StringBuilder();
            foreach (var c in data)
                sb.append_c((char) c);

            if (!sb.str.validate())
                return null;

            var doc = new Parser.immutable_new();

            try {
                doc.load_from_data(sb.str);
                unowned Json.Node? root = doc.get_root();

                if (root == null)
                    return null;

                unowned Json.Array? array = root.get_object().get_array_member("posts");

                if (array == null || array.get_length() < 1)
                    return null;

                posts = new Post[array.get_length()];
                int nonNull = 0;

                for (uint i = 0; i < array.get_length(); ++i) {
                    Posts.Post? post = null;
                    if (parse_json_post_from_node(array.get_element(i), out post))
                        posts[nonNull++] = post;
                }

                if (nonNull > 0) {
                    posts.resize(nonNull);
                    return posts;
                } else {
                    return null;
                }
            } catch (Error err) {
                return null;
            }
        }
    }
}
