/*
 * Copyright (C) 2022  Dawid Duma (pixel96x) <contact@pixel96x.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022 Dawid Duma (pixel96x) <contact@pixel96x.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using GLib;

namespace BluePaw
{
    [GtkTemplate (ui="/com/pixel96x/BluePaw/ui/page_homepage.ui")]
    public sealed class PageHomepage : Adw.Bin, IPage
    {
        public Icon icon { get; construct; }
        public PageManager page_manager { get; construct; }
        public BluePaw.PageType page_type { get; construct; }
        public string title { get; protected set; }

        [GtkChild]
        private unowned Gtk.Entry search_entry;
        [GtkChild]
        private unowned Gtk.Button search_btn;

        public PageHomepage(PageManager page_manager)
        {
            Object(
                icon: new ThemedIcon("go-home-symbolic"),
                page_manager: page_manager,
                page_type: PageType.HOMEPAGE,
                title: "Homepage"
            );
        }

        [GtkCallback]
        private void search_entry_activate_cb(Gtk.Entry entry)
        {
            this.search_btn.clicked();
        }

        [GtkCallback]
        public async void search_btn_clicked_cb(Gtk.Button button)
        {
            this.search_entry.sensitive = this.search_btn.sensitive = false;

            Posts.fetch_posts.begin("https://e621.net/posts.json?tags=" + this.search_entry.buffer.text, null, (obj, res) => {
                var posts = Posts.fetch_posts.end(res);
                this.page_manager.load_search_page(posts, this.search_entry.buffer.text);
                this.search_entry.sensitive = this.search_btn.sensitive = true;
            });
        }

        [GtkCallback]
        public async void latest_posts_btn_clicked_cb(Gtk.Button button)
        {
            button.sensitive = false;

            Posts.fetch_posts.begin("https://e621.net/posts.json", null, (obj, res) => {
                var posts = Posts.fetch_posts.end(res);
                this.page_manager.load_search_page(posts);
                button.sensitive = true;
            });
        }

        [GtkCallback]
        public async void last_24h_btn_clicked_cb(Gtk.Button button)
        {
            button.sensitive = false;
            var date = new DateTime.now_local();
            var sb = new StringBuilder();
            sb.append("https://e621.net/explore/posts/popular.json?date=");
            sb.append(date.get_year().to_string());
            sb.append("-");
            sb.append(date.get_month().to_string());
            sb.append("-");
            sb.append(date.get_day_of_month().to_string());
            sb.append("&scale=day");

            Posts.fetch_posts.begin(sb.str, null, (obj, res) => {
                var posts = Posts.fetch_posts.end(res);
                this.page_manager.load_search_page(posts);
                button.sensitive = true;
            });
        }

        [GtkCallback]
        public async void last_7d_btn_clicked_cb(Gtk.Button button)
        {
            button.sensitive = false;
            var date = new DateTime.now_local();
            var sb = new StringBuilder();
            sb.append("https://e621.net/explore/posts/popular.json?date=");
            sb.append(date.get_year().to_string());
            sb.append("-");
            sb.append(date.get_month().to_string());
            sb.append("-");
            sb.append(date.get_day_of_month().to_string());
            sb.append("&scale=week");

            Posts.fetch_posts.begin(sb.str, null, (obj, res) => {
                var posts = Posts.fetch_posts.end(res);
                this.page_manager.load_search_page(posts);
                button.sensitive = true;
            });
        }

        [GtkCallback]
        public async void last_month_btn_clicked_cb(Gtk.Button button)
        {
            button.sensitive = false;
            var date = new DateTime.now_local();
            var sb = new StringBuilder();
            sb.append("https://e621.net/explore/posts/popular.json?date=");
            sb.append(date.get_year().to_string());
            sb.append("-");
            sb.append(date.get_month().to_string());
            sb.append("-");
            sb.append(date.get_day_of_month().to_string());
            sb.append("&scale=month");

            Posts.fetch_posts.begin(sb.str, null, (obj, res) => {
                var posts = Posts.fetch_posts.end(res);
                this.page_manager.load_search_page(posts);
                button.sensitive = true;
            });
        }
    }
}
