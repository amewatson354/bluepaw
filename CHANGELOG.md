## [**v0.2.0**](https://gitlab.com/pixel96x/bluepaw/-/compare/v0.1.0...v0.2.0) (2022-06-30)

### Added

* Basic preferences window
* Support for themes; app can use light, dark or system default theme
* Posts search bar in a search page
* Posts can now be opened in new tab with MMB
* Basic menubar (for future compatibility)

### Changed

* Minor UI improvements

### Fixed

* Blurry thumbnails in search results
* Images being too small in post view
* Minor UI fixes

## **v0.1.0** (2022-06-02)

* initial release
