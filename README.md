# Bluepaw

**Bluepaw** is a desktop client for [e621.net](https://e621.net/).

# Building

## Required libraries

Library | Version
--- | ---
glib2 | >= 2.68.0
GTK | >= 4.6.0
libadwaita | >= 1.1.0
json-glib | >=1.6.0
soup | >= 3.0.0

## Additional tools

Tool | Version
--- | ---
meson | >= 0.59.0
vala | >= 0.56.0

## Compiling application

```sh
meson setup --buildtype=debugoptimized --prefix=/usr/local/ build
cd build
meson compile
# to install into system:
sudo meson install --no-rebuild
```

# Screenshots

![Homepage](/data/screenshots/s1_homepage.png?raw=true "Homepage")
![Search results](/data/screenshots/s2_search_results.png?raw=true "Search results")
![Post view](/data/screenshots/s3_post_view.png?raw=true "Post view")
![Light theme](/data/screenshots/s4_light_theme.png?raw=true "Light theme")
![Tabs](/data/screenshots/s5_tabs.png?raw=true "Tabs")

# License

This application is licensed under a GPLv3 license. See [COPYNING.txt](COPYING.txt) for details.

**NOTE**: by using this application You accept the [e621.net Terms of Service](https://e621.net/static/terms_of_service)

# Other

* Authors: see [AUTHORS.md](AUTHORS.md)
* Changelog: see [CHANGELOG.md](CHANGELOG.md)
* Planned features: see [TODO.md](TODO.md)
